import org.hibernate.Session;

public class Principal {

	public static void main(String[] args) {
		
		Session session=HibernateUtilities.getSessionFactory().openSession();
		
		session.beginTransaction();
		Usuario u = new Usuario("Juanmi");
		session.save(u);
		
		session.getTransaction().commit();
		
		/*session.beginTransaction();
		Usuario u2 = session.get(Usuario.class, 1);
		System.out.println("Hemos recuperado "+u.getNombre());
		
		session.close();*/
		HibernateUtilities.getSessionFactory().close();

	}

}
